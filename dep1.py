import os

from utils.iam_stuff import delete_policy, delete_role, create_policy, create_role
from utils.lambda_stuff import delete_function, create_function
from utils.s3_stuff import delete_bucket, create_bucket, copy_code_to_bucket, put_notification_on_bucket

# -- change this
PREFIX  = '6756346'
ACCOUNT = os.getenv('AWS_ACCOUNT')


# -- no changes below
code_bucket_name = f'{PREFIX}-code-bucket'
data_bucket_name = f'{PREFIX}-data-bucket'
data_bucket_arn  = f'arn:aws:s3:::{data_bucket_name}'

code_object_name = 'my-code-object.zip'
code_location    = 'code/my-code-object.zip'

lambda_name = f'{PREFIX}-lambda_function'
lambda_arn  = f'arn:aws:lambda:eu-central-1:{ACCOUNT}:function:{lambda_name}'

policy_name = f'{PREFIX}-policy'
policy_arn  = f'arn:aws:iam::{ACCOUNT}:policy/{policy_name}'

role_name   = f'{PREFIX}-role'
role_arn    = f'arn:aws:iam::{ACCOUNT}:role/{role_name}'


# -- delete old stuff
delete_bucket(bucket_name=code_bucket_name)
delete_bucket(bucket_name=data_bucket_name)

delete_function(lambda_name=lambda_name)

delete_role(role_name=role_name)
delete_policy(policy_arn=policy_arn)


# -- create new stuff
response = create_bucket(bucket_name=code_bucket_name)
response = create_bucket(bucket_name=data_bucket_name)

response = copy_code_to_bucket(
    code_file=code_location,
    code_bucket_name=code_bucket_name,
    code_object_name=code_object_name
)


response = create_policy(policy_name=policy_name)

response = create_role(
    policy_arn=policy_arn,
    role_name=role_name
)

response = create_function(
    lambda_name=lambda_name,
    role_arn=role_arn,
    code_bucket_name=code_bucket_name,
    code_object_name=code_object_name,
    data_bucket_arn=data_bucket_arn,
    account=ACCOUNT
)

response = put_notification_on_bucket(
    bucket_name=data_bucket_name,
    lambda_arn=lambda_arn
)
