import boto3
import json

import logging
logging.basicConfig(
    filename='logging.log',
    level=logging.INFO,
    filemode='w',
    format='%(asctime)s %(message)s'
)

iam_client = boto3.client('iam')
""" :type: pyboto3.iam """


# -- create trust relation document
trust_relationship_policy = {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "lambda.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}

# Create a policy
my_managed_policy = {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "logs:CreateLogGroup",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "dynamodb:DeleteItem",
                "dynamodb:GetItem",
                "dynamodb:PutItem",
                "dynamodb:Scan",
                "dynamodb:UpdateItem"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "xray:PutTraceSegments",
                "xray:PutTelemetryRecords"
            ],
            "Resource": "*"
        }
    ]
}



def create_policy(policy_name):

    try:
        policy_arn = iam_client.create_policy(
            PolicyName=policy_name,
            PolicyDocument=json.dumps(my_managed_policy)
        )['Policy']['Arn']
    except:
        logging.error(f' *** problem creating policy {policy_name}')
    else:
        logging.info(f' --- created policy {policy_name}')

    return policy_arn


def create_role(policy_arn, role_name):

    try:
        role_arn = iam_client.create_role(
            RoleName=role_name,
            AssumeRolePolicyDocument=json.dumps(trust_relationship_policy),
            Description='my role description',
            MaxSessionDuration=3600,
            PermissionsBoundary=policy_arn
    )['Role']['Arn']
    except:
        logging.error(f' *** problem creating role {role_name}')
    else:
        logging.info(f' --- created role {role_name}')


    return role_arn



def delete_role(role_name):
    try:
        iam_client.delete_role(RoleName=role_name)
    except:
        logging.error(f' *** failed to delete role {role_name}')
    else:
        logging.info(f' --- deleted role {role_name}')


def delete_policy(policy_arn):
    try:
        iam_client.delete_policy(PolicyArn=policy_arn)
    except:
        logging.error(f' *** failed to delete policy {policy_arn.split("/")[-1]}')
    else:
        logging.info(f' --- deleted policy {policy_arn}')
