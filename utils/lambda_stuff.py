import boto3
import pyboto3

import logging
logging.basicConfig(
    filename='logging.log',
    level=logging.INFO,
    filemode='w',
    format='%(asctime)s %(message)s'
)

lambda_client = boto3.client('lambda')
""" :type: pyboto3.lambda """

def create_function(lambda_name, role_arn, code_bucket_name, code_object_name, data_bucket_arn, account):

    try:
        response1 = lambda_client.create_function(
            FunctionName=lambda_name,
            Runtime='python3.8',
            Role=role_arn,
            Handler='my-code-object.event_handler',
            Code={
                # 'ZipFile': b'bytes',
                'S3Bucket': code_bucket_name,
                'S3Key': code_object_name,
                # 'S3ObjectVersion': 'string'
            },
            Description='my boto3 deployed lambda function',
            Timeout=30,
            MemorySize=512,
            Publish=True,
            Environment={
                'Variables': {
                    'env_var1': 'env_val1'
                }
            },
            # TracingConfig={
            #     'Mode': 'Active'
            # },
            Tags={
                'my_tag': 'my_value'
            }
        )

        response2 = lambda_client.add_permission(
            FunctionName=lambda_name,
            StatementId='1',
            Action='lambda:InvokeFunction',
            Principal='s3.amazonaws.com',
            SourceArn=data_bucket_arn,
            SourceAccount=account
        )
    except:
        logging.error(f' *** problem deploying lambda function {lambda_name}')
    else:
        logging.info(f' --- deployed lambda function {lambda_name}')

    return response1, response2


def delete_function(lambda_name):

    try:
        response =  lambda_client.delete_function(
            FunctionName=lambda_name
        )
    except:
        logging.error(f' *** failed to delete function {lambda_name}')
        response = ''
    else:
        logging.info(f' --- deleted function {lambda_name}')

    return response
