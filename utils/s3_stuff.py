import boto3
import pyboto3

import logging
logging.basicConfig(
    filename='logging.log',
    filemode='w',
    format='%(levelname)s: %(message)s %(asctime)s %(message)s',
)


s3_client = boto3.client('s3')
""" :type: pyboto3.s3 """

def list_buckets():
    return [b['Name'] for b in s3_client.list_buckets()['Buckets']]


def delete_bucket(bucket_name):

    def empty_bucket(bucket_name):
        try:
            for key in s3_client.list_objects(Bucket=bucket_name)['Contents']:
                s3_client.delete_object(
                    Bucket=bucket_name,
                    Key=key['Key']
                )
        except:
            pass


    if bucket_name in list_buckets():

        try:
            empty_bucket(bucket_name)
            s3_client.delete_bucket(
                Bucket=bucket_name
            )
        except:
            logging.info(f' *** issue deleting bucket {bucket_name}')
        else:
            logging.info(f' --- deleted bucket {bucket_name}')

    else:
        logging.info(f' *** bucket {bucket_name} does not exist, not deleting')


def create_bucket(bucket_name):
        if not bucket_name in list_buckets():
            try:
                s3_client.create_bucket(
                    Bucket=bucket_name,
                    CreateBucketConfiguration={'LocationConstraint': 'eu-central-1'})
            except:
                logging.error(f'  ** issue creating bucket {bucket_name}')
            else:
                logging.error(f' --- created bucket {bucket_name}')

        else:
            logging.info(f'  ** bucket {bucket_name} exists: cannot create')


def copy_code_to_bucket(code_file, code_bucket_name, code_object_name):

    with open(code_file, 'rb') as f:
        s3_client.upload_fileobj(f, code_bucket_name, code_object_name)


def put_notification_on_bucket(bucket_name,lambda_arn):
    try:
        response = s3_client.put_bucket_notification_configuration(
            Bucket=bucket_name,
            NotificationConfiguration={
                'LambdaFunctionConfigurations': [
                    {
                        'Id': 'my_trigger_id',
                        'LambdaFunctionArn': lambda_arn,
                        'Events': [
                            's3:ObjectCreated:*'
                        ],
                        # 'Filter': {
                        #     'Key': {
                        #         'FilterRules': [
                        #             {
                        #                 'Name': 'prefix'|'suffix',
                        #                 'Value': 'string'
                        #             },
                        #         ]
                        #     }
                        # }
                    },
                ]
            }
        )
    except:
        logging.info(f' *** issue deploying lambda notification for {lambda_arn.split("/")[-1]}')
    else:
        logging.info(f' --- deployed lambda notification {lambda_arn.split("/")[-1]}')


    return response